# C++ TASKS #

Here the repository, where tasks for MIPT OOP course will be stored. 

## Structure of repository ##

* All tasks from lections will be in the _tasks_ directory. 
* The __Big Project__ will be in the _project_ directory. 

### Notes ###

With some probability, the project will be added as a __git submodule__, so the clonnig should be done with something like

```
#!bash

git clone --recursive https://Mikle_Bond@bitbucket.org/Mikle_Bond/oop_lections.git oop

```