#include <iostream>

class SuperString
{
public: 
	// impicit type cast
	SuperString(const char *str)
	{
		length_ = countLength(str);
		if (length_ == 0)
			size_ = 10;
		else
			size_ = length_ + 1;
		str_ = new char [size_];
		int i = 0;
		while ((str_[i] = str[i]) != '\0') ++i;
	} 

	// default empty string
	explicit SuperString(size_t size = 10) 
	:
		length_(0),
		size_(size),
		str_(new char[size_ + 1])
	{
		str_[0] = '\0';	
	}
	
	SuperString(const SuperString & rhs) 
	:
		SuperString(rhs.getString())
	{}

	~SuperString()
	{
		delete [] str_;
	}

	// Only one string to another. For now.
	SuperString operator + (const SuperString & rhs) const
	{
		SuperString temp(length_ + rhs.length_);
		int i, j;
		for (i = 0; i < length_; ++i)
			temp.str_[i] = str_[i];
		for (j = i, i = 0; i < rhs.length_; ++i, ++j)
			temp.str_[j] = rhs.str_[i];
		temp.str_[j] = '\0';
		temp.length_ = length_ + rhs.length_;
		return temp;
	}

	const char * getString() const { return str_; }
	int getLength() const { return length_; }
private:
	int countLength(const char * s) 
	{
		int i = 0;
		while (s[i++] != '\0');
		return i - 1;
	}
	int length_;
	int size_;
	char * str_;
};

using std::cout;
using std::endl;

int main () 
{
	SuperString h("Hello,");
	SuperString w("World!");
	SuperString out(h + " " + w);
	cout << h.getLength() << endl;
	cout << w.getLength() << endl;
	cout << (h+w).getString() << endl;
	std::cout << out.getString() << std::endl;
	return 0;
}

