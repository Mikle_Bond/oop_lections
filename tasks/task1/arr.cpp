#include <iostream>
using std::cout;
using std::endl;

class Array
{
public:
	explicit Array(int size = 10)
	:
		size_(size),
		arr_(new int [size_])
	{}
	Array(int size, int * arr)
	:
		Array(size)
	{
		for (int i = 0; i < size_; ++i)
			arr_[i] = arr[i];
	}
	Array(const Array & rhs)
	:
		Array(rhs.size_, rhs.arr_)
	{}


	~Array()
	{
		delete [] arr_;
	}

	int getSize() const
	{
		return size_;
	}

	int & operator[](int i)
	{
		return arr_[i];
	}

	Array operator+(const Array & rhs) const
	{
		Array temp(size_ + rhs.size_);
		int *t = temp.arr_;
		for (int i = 0; i < size_; ++i)
			*t++ = arr_[i];
		for (int i = 0; i < rhs.size_; ++i)
			*t++ = rhs.arr_[i];
		return temp;
	}

	Array & operator=(const Array & rhs) 
	{
		Array temp(rhs);
		swap(temp);
		return *this;
	}

	void swap(Array & rhs) 
	{
		int x, *y;
		x = size_; y = arr_;
		size_ = rhs.size_; arr_ = rhs.arr_;
		rhs.size_ = x; rhs.arr_ = y;
	}

private:
	int size_;
	int * arr_;
};

int main ()
{
	int a_int[] = { 1, 2, 38, 32, 29 };
	Array a(5, a_int);
	Array b = a;
	Array c;
	c = a + b;
	for (int i = 0; i < c.getSize(); ++i)
		cout << i << " -- " << c[i] << endl;
	return 0;
}

