#pragma once
#include <iostream>

template <class T>
class SmartPointer {
	T *obj;
	mutable unsigned *ref_count;
	void clear() {
		if (obj == nullptr)
			std::cerr 
				<< "[WARN] Deleting SmartPointer with nullptr value." 
				<< std::endl;
		delete obj;
		delete ref_count;
		obj = nullptr;
		ref_count = nullptr;
	}
	void increase()
	{
		*ref_count += 1;
	}
	void decrease()
	{
		*ref_count -= 1;
		if (*ref_count == 0) 
			clear();
	}
public:
	SmartPointer(T *object)
	{
		obj = object;
		ref_count = new unsigned;
		*ref_count = 1;
	}

	SmartPointer(const SmartPointer<T> & sptr)
	{
		obj = sptr.obj;
		ref_count = sptr.ref_count;
		increase();
	}

	~SmartPointer()
	{
		decrease();
	}

	SmartPointer<T> & operator=(const SmartPointer<T> & sptr)
	{
		if (&sptr == this) 
			return *this;
		if (obj == sptr.obj) 
			return *this;
		decrease();
		ref_count = sptr.ref_count;
		obj = sptr.obj;
		increase();
		return *this;
	}

	T * const getObject() 
	{
		return obj;
	}

	T & operator* () 
	{
		return *obj;
	}
};

