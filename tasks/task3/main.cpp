#include <iostream>
#include "smartPtr.hpp"

using std::cout;
using std::endl;

struct Holder 
{
	SmartPointer<int> ptr;
	Holder() : ptr(nullptr) {}
};

template <typename ...Args>
void function(SmartPointer<int> arr, Holder & first, Args&...other)
{
	first.ptr = arr;
	function(arr, other...);
}

template <>
void function(SmartPointer<int> arr, Holder & first)
{
	first.ptr = arr;
}

void creator(Holder & first, Holder & second, Holder & third)
{
	SmartPointer<int> arr = new int[10] {55, 67, 74, 62, 15, 48, 92, 15, 64, 11};
	function(arr, first, second, third);
}

auto main() -> int
{
	Holder f;
       	Holder *s = new Holder();
	Holder *t = s;
	creator(f, *s, *t); 

	for (int i = 0; i < 10; ++i)
		cout << ((s->ptr).getObject())[i] << " ";
	cout << endl;

	delete s;
	s = t = nullptr;

	for (int i = 0; i < 10; ++i)
		cout << ((f.ptr).getObject())[i] << " ";
	cout << endl;

	return 0;
}

