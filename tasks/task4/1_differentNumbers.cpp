#include <iostream>
#include <set>
#include <string>
#include <cstdlib>

void printHelp(void) {}

int main(int argc, char *argv[])
{
	if (argc == 1) {
		printHelp();
	} 
	bool long_text = true;
	if (std::string(argv[1]) == "-f") 
		long_text = false;
	std::set<int> nums;
	for (int i = (long_text ? 1 : 2); i < argc; ++i) {
		nums.insert(std::atoi(argv[i]));
	}
	if (long_text) {
		std::cout << "Reciving numbers..." << std::endl;
		system("sleep 1");
		std::cout << "Analizing..." << std::endl;
		system("sleep 1");
		std::cout << "Counting different numbers..." << std::endl;
		system("sleep 1");
		std::cout << "Drinking vodka..." << std::endl;
		system("sleep 5");
		std::cout << "And the answer you want..." << std::endl;
		system("sleep 1");
		std::cout << "is..." << std::endl;
		system("sleep 2");
		std::cout << "42!" << std::endl;
		system("sleep 5");
		std::cout << "Just a joke." << std::endl;
		system("sleep 1");
		std::cout << "It is " << nums.size() << std::endl;
	} else {
		std::cout << nums.size() << std::endl;
	}
	return 0;
}
