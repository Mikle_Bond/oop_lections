#include <vector>
#include <list>
#include <deque>
#include <set>
#include <map>
#include <string>
#include <iostream>
using namespace std;
typedef std::multimap<int, string> myMap;

void showVector()
{
	cout << "vector" << endl;
	vector<int> coll; // vector container for integer elements
	// append elements with values 1 to 6
	for (int i = 1; i <= 6; ++i)
		coll.push_back(i);
	// print all elements followed by a space
	for (size_t i = 0; i < coll.size(); ++i)
		cout << coll[i] << " ";
	cout << endl;
}

void showList()
{
	cout << "list" << endl;
	list<char> coll; // list container for character elements
	// append elements from �a� to �z�
	for (char c = 'a'; c <= 'z'; ++c)
		coll.push_back(c);
	// print all elements:
	list<char>::iterator iter;
	for (iter = coll.begin(); iter != coll.end(); iter++)
		cout << *iter << " ";
	cout << endl;
}

void showDeq()
{
	cout << "deque" << endl;
	deque<float> coll; // deque container for floating-point elements
	// insert elements from 1.1 to 6.6 each at the front
	for (int i = 1; i <= 6; ++i)
		coll.push_front(i * 1.1); // insert at the front
	// print all elements followed by a space
	for (size_t i = 0; i < coll.size(); ++i)
		cout << coll[i] << " ";
	cout << endl;
}

void showSet()
{
	cout << "set" << endl;
	set<string> cities;
	cities.insert("Braunschweig");
	cities.insert("Hanover");
	cities.insert("Frankfurt");
	cities.insert("New York");
	cities.insert("Chicago");
	cities.insert("Toronto");
	cities.insert("Paris");
	cities.insert("Frankfurt");
	// print each element:
	set<string>::iterator iter;
	for (iter = cities.begin(); iter != cities.end(); iter++)
		cout << *iter << " ";
	cout << endl;
	// insert additional values:
	cities.insert("London");
	cities.insert("Munich");
	cities.insert("Hanover");
	cities.insert("Braunschweig");
	// print each element:
	for (iter = cities.begin(); iter != cities.end(); iter++)
		cout << *iter << " ";
	cout << endl;
}

void showMultiset()
{
	cout << "multiset" << endl;
	multiset<string> cities;
	cities.insert("Braunschweig");
	cities.insert("Hanover");
	cities.insert("Frankfurt");
	cities.insert("New York");
	cities.insert("Chicago");
	cities.insert("Toronto");
	cities.insert("Paris");
	cities.insert("Frankfurt");
	// print each element:
	multiset<string>::iterator iter;
	for (iter = cities.begin(); iter != cities.end(); iter++)
		cout << *iter << " ";
	cout << endl;
	// insert additional values:
	cities.insert("London");
	cities.insert("Munich");
	cities.insert("Hanover");
	cities.insert("Braunschweig");
	// print each element:
	for (iter = cities.begin(); iter != cities.end(); iter++)
		cout << *iter << " ";
	cout << endl;
	iter = cities.find("Dolgopa");
	if (iter == cities.end())
		cout << "not found" << endl;
	else
		cout << "found " << *iter << endl;
}

void showMultimap()
{
	cout << "multimap" << endl;
	myMap coll; // container for int/string values
	// insert some elements in arbitrary order
	// - a value with key 1 gets inserted twice
	coll.insert(myMap::value_type(5, "tagged"));
	coll.insert(myMap::value_type(2, "a"));
	coll.insert(myMap::value_type(1, "this"));
	coll.insert(myMap::value_type(4, "of"));
	coll.insert(myMap::value_type(6, "strings"));
	coll.insert(myMap::value_type(1, "is"));
	coll.insert(myMap::value_type(3, "multimap"));
	// print all element values
	// - element member second is the value
	multimap<int, string>::iterator iter;
	for (iter = coll.begin(); iter != coll.end(); iter++)
		cout << iter->first << " -->" << iter->second << endl;
	cout << endl;
}

int main()
{
	showVector();
	showList();
	showDeq();
	showSet();
	showMultiset();
	showMultimap();
	return 0;
}
