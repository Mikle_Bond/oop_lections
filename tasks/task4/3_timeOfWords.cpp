#include <map>
#include <iostream>
#include <string>
#include <fstream>

void printHelp(void)
{
	std::cout << "Senpai, BAKA!" << std::endl;
}

int main(int argc, const char * argv[])
{
	if (argc != 2) {
		printHelp();
	}

	std::ifstream file;
	file.open(argv[1], std::ios::in);

	std::map<std::string, int> keys;
	std::string word;

	while (!file.eof()) {
		file >> word;
		if (keys.count(word) == 0) {
			keys[word] = 1;
		} else {
			keys[word]++;
		}
	}
	keys[word]--;

	for (auto it = keys.cbegin(); it != keys.cend(); ++it) {
		std::cout << "'" << it->first << "' was met " << it->second << " times" << std::endl;
	}

	return 0;
}

