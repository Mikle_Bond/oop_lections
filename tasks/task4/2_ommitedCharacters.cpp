#include <set>
#include <iostream>
#include <fstream>
#include <array>

void printHelp(void) {}

int main(int argc, const char *argv[])
{
	if (argc != 2) {
		printHelp();
		return 0;
	}
	char c;
	std::ifstream file;
	std::set<char> knownChars;
	file.open(argv[1], std::ios::in | std::ios::binary);

	while (!file.eof()) {
		file >> c;
		if (std::isalpha(c)) {
			knownChars.insert(std::toupper(c));
		}
	}

	std::cout << "Chars, that haven't been meet:\n";
	for(char i = 'A'; i <= 'Z'; ++i)
		if(knownChars.count(i) == 0)
			std::cout << i << ' ';
	std::cout << std::endl;
	
	return 0;
}
