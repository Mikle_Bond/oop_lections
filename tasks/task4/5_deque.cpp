#include <deque>
#include <iostream>
#include <fstream>
#include <string>

void printHelp(void) {}

std::string & tolower(std::string & str)
{
	for (auto it : str) 
		it = std::tolower(it);
	return str;
}

int main(int argc, const char *argv[])
{
	if (argc != 2) {
		printHelp();
		return 0;
	}
	size_t size;
	std::string word;
	std::ifstream file;
	file.open(argv[1], std::ios::in | std::ios::binary);
	auto deque_ptr = new std::deque<std::string>;
	auto & deque = *deque_ptr;

	// Read words!
	while (!file.eof()) {
		file >> word;
		deque.push_back(std::move(word));
	}

	// Lowercase them!
	size = deque.size();
	for (size_t i = 0; i < size; ++i) {
		deque.push_back(::tolower(deque.front()));
		deque.pop_front();
	}

	// Kill chinece copies!
	{
		std::deque<std::string> temp;
		for (size_t i = 0; i < size; ++i) {
			auto s = deque.front();
			for (auto it = deque.begin(); it != deque.end();) {
				if (*it == s) {
					it = deque.erase(it);
				} else {
					++it;
				}
			}
			temp.push_back(s);
		}
		deque.swap(temp);
	}
	// Show, who is left
	for (auto it : deque)
		std::cout << it << " ";
	std::cout << std::endl;

	// Kill'em too!!!
	delete deque_ptr;
	file.close();
	
	return 0;
}
