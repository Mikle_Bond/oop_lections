// Problem.  Approach is now more space efficient, but casting and type
// checking are required to coerce the compiler.  The abstraction needs to
// be improved so that Primitive and Composite can be treated
// transparently in a sibling context, while still maintaining their
// specialization.
//
// Assignment.
// o Create a class Component to serve as a base class.  Primitive and
//   Composite should inherit from Component.
// o Move anything that is (or needs to be) common in both Primitive and
//   Composite up into Component.
// o Currently class Composite is coupled to itself (the argument to add() and
//   the children private data member).  It needs to be coupled only to its
//   abstract base class.
// o You can now remove: NodeType, reportType(), the casting in main() and
//   Composite::traverse(), and the "type checking" in Composite::traverse()

#include <iostream>
enum NodeType { LEAF, INTERIOR };

using namespace std;

class Component
{
public:
	Component(int value, NodeType type) 
	:
		value_(value), type_(type)
       	{}

	virtual ~Component() = 0;

	NodeType reportType() {
		return type_;
	}

	virtual void traverse() {
		cout << value_ << " ";
	}

protected:
	int value_;
	NodeType type_;
};

Component::~Component() {}

class Primitive : public Component
{
public:
	Primitive(int val)
	:
	       Component(val, LEAF)
	{}
	~Primitive() {}
};

class Composite : public Component
{
public:
	Composite(int val) 
	: 
		Component(val, INTERIOR)
	{
		total = 0;
	}
	
	~Composite() {
//		for (int i = 0; i < total; ++i) {
//			delete children[i];
//		}
	}

	void add(Component *c)
	{
		children[total++] = c;
	}
	void traverse() override
	{
		cout << value_ << " ";
		for (int i = 0; i < total; i++)
			children[i]->traverse();
	}
private:
	int total;
	Component *children[99];
};

int main(void)
{
	Composite first(1), second(2), third(3);

	first.add(&second);
	first.add(&third);
	first.add(new Primitive(4));
	second.add(new Primitive(5));
	second.add(new Primitive(6));
	third.add(new Primitive(7));
	first.traverse();
	cout << endl;
	return 0;
}

// 1 2 5 6 3 7 4
