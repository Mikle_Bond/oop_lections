// Purpose.  Adapter design pattern lab
//
// ������.  ����� ������������ "stack machine", ��� �� ��-����������
// ������ Stack.  �� ������ �� �������� ������������ ���� "legacy"
// ����� ��� ������ ��� ������ ������ Queue, �� ������� "�������������� ����������"
// ����� ������ � ����� �����������.
//
// �������.
// o ����� Queue ������ ��������� "�������" ���������� Stack.
// o ������ Queue ��������� ������ ctor, dtor, enque, deque, � isEmpty.  ������ ��
//   ���� ������� ��������� ��������� �� ���������� Queue � ����������
//   Stack.
// o ���� �� ���������� Queue::enque() �� Stack::push(), �� ����������� ���� ���������
//   ���� ��� ��������� ���������� ������ Queue::deque().

#include <iostream>

namespace {
struct StackStruct {
	int *	array;
	int	sp;
	int	size;
};
typedef StackStruct Stack;

static void initialize(Stack *s, int size)
{
	s->array = new int[size];
	s->size = size;
	s->sp = 0;
}
static void cleanUp(Stack *s)
{
	delete s->array;
}
static int isEmpty(Stack *s)
{
	return s->sp == 0 ? 1 : 0;
}
static int isFull(Stack *s)
{
	return s->sp == s->size ? 1 : 0;
}
static void push(Stack *s, int item)
{
	if (!isFull(s)) s->array[s->sp++] = item;
}
static int pop(Stack *s)
{
	if (isEmpty(s)) return 0;
	else            return s->array[--s->sp];
}
}
class Queue {
	Stack * inner_stack_;
public:
	Queue(int number) {
		inner_stack_ = new Stack;
		initialize(inner_stack_, number);
	}
	
	 ~Queue() {
		::cleanUp(inner_stack_);	
	}

	int isEmpty() const {
		return ::isEmpty(inner_stack_);
	}

	int isFull() const {
		return ::isFull(inner_stack_);
	}

	void enque(int elem) {
		::push(inner_stack_, elem);
	}

	int deque() {
		Stack * tmp = new Stack;
		::initialize(tmp, inner_stack_->size);
		while(! ::isEmpty(inner_stack_)) {
			::push(tmp, ::pop(inner_stack_));
		}
		int ret = ::pop(tmp);
		while(! ::isEmpty(tmp)) {
			::push(inner_stack_, ::pop(tmp));
		}
		::cleanUp(tmp);
		return ret;
	}
};

using std::cout;
using std::endl;

int main()
{
	Queue queue(15);

	for (int i = 0; i < 25; i++) queue.enque(i);
	while (!queue.isEmpty())
		cout << queue.deque() << " ";
	cout << endl;
	return 0;
}

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
