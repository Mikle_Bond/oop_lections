#include <iostream>

template <typename T>
class List;

template <class TNode>
class Iterator
{
	/* Helper class to provide pointer like facilities around a node */
	using value_type = typename TNode::value_type;
	friend class List<value_type>;
	TNode* pNode;
	List<value_type> * father;

	Iterator(TNode* _pNode, List<value_type> * uncle) 
	: 
		pNode(_pNode),
		father(uncle)
	{}
public:
	value_type operator * () {
		return *(pNode->data_field);
	}
	Iterator<TNode> & operator ++ ()
	{
		pNode = pNode->next;
		if (!pNode) throw;
		return *this;
	}
	Iterator<TNode> operator + (unsigned i) 
	{
		while (i--) 
			operator ++ ();
		return *this;
	}
	bool operator != (const Iterator<TNode> & rhs) {
		return !(pNode == rhs.pNode);
	}
	~Iterator() = default;
};

template <typename T>
class Node
{
	friend class List<T>;
	friend class Iterator<Node<T> >;

	T * data_field;
	Node<T> * next;
	List<T> * father;

	Node(const T & data, Node<T> * next_node, List<T> * uncle) 
	:
		data_field(new T(data)),
		next(next_node),
		father(uncle)
	{
		
	}

	Node(List<T> * uncle) 
	:
		data_field(nullptr),
		next(nullptr),
		father(uncle)
	{
	
	}
	
	~Node() 
	{
		delete data_field;
		delete next;
	}

public:
	typedef T value_type;
};

template <typename T>
class List
{
	Node<T>* first;
	Node<T> * last;
	Node<T> * dummy;

public:
	typedef Iterator<Node<T> > iterator;
	typedef T 		  value_type;

	List() 
	{
		dummy = first = last = new Node<T>(this);
//		std::cout << "ctor" << std::endl;
	}

	~List() 
	{
//		std::cout << "dtor" << std::endl;
		delete first;
	}

	void push_back(T data)
	{
//		std::cout << "pback" << std::endl;
		Node<T> * temp = new Node<T>(data, dummy, this);
		if (last == dummy) {
			first = temp;
		} else {
			last->next = temp;
		}
		last = temp;
	}

	void push_front(T data)
	{
//		std::cout << "pfront" << std::endl;
		Node<T> * temp = new Node<T>(data, first, this);
		first = temp;
		if (last == dummy) {
			last = temp;
		}
	}

	iterator begin() { 
//		std::cout << "begin" << std::endl;
		return iterator(first, this); 
	}
	iterator end() { 
//		std::cout << "end" << std::endl;
		return iterator(dummy, this); 
	}

	bool erase(iterator& _iNode) //True for success, vice versa
	{
		if (_iNode.father != this)
			return false;
		if (_iNode.pNode == dummy)
			return false;

		auto cur = _iNode.pNode;
		auto nxt = cur->next;
		if (cur == first) {
			// ommit search for the previous element
			first = nxt;
		} else  {
			auto prv = first;
			while (prv->next != cur) 
				prv = prv->next;
			prv->next = nxt;

			if (cur == last) {
				last = nxt;
			}
		}
		cur->next = nullptr; // neded for correct delete
		delete cur;
		_iNode.pNode = nxt;
		return true;
	}
};

#define magic_print \
	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter) \
	{ \
		std::cout << (*iter) << std::endl; \
	} \
// define

int main(void)
{
	List<int> list;
	magic_print
	list.push_back(3);
	list.push_back(4);
	list.push_front(2);
	list.push_front(1);

	/*Print all elements*/
	magic_print

	/*Delete second element and reprint*/
	List<int>::iterator tmp = list.begin() + 1;
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	/*Now delete first node and print again*/
	tmp = list.begin();
	list.erase(tmp);

	for (List<int>::iterator iter = list.begin(); iter != list.end(); ++iter)
	{
		std::cout << (*iter) << std::endl;
	}

	//List object takes care of deletion for us.
	return 0;
}
