#include<iostream>
#include<vector>

#include<iterator>

using namespace std;

void inputIterator(){
        cout << "input iter" << endl;
        vector<int> v;
        for (int i = 0; i < 10; i++)
            v.push_back(i*2);
        vector<int>::iterator it = v.begin();
        while (it != v.end())
            cout << *it++ << " ";
        cout << endl;
}

void outIterator(){
        cout << "output iter" << endl;
        vector<int> v(5);
        vector<int>::iterator out = v.begin();
        *out++ = 10;
        *out++ = 11;
        *out++ = 12;
        *out++ = 13;
        *out = 14;
        vector<int>::iterator it = v.begin();
        while (it != v.end())
            cout << *it++ << " ";
        cout << endl;
}

void forwardIter(){
    cout << "forward iter" << endl;
    vector<int> v(5);
    vector<int>::iterator it = v.begin();
    vector<int>::iterator saveIt = it;
    *it++ = 10;
    *it++ = 11;
    *it++ = 12;
    *it++ = 13;
    *it= 14;
    while (saveIt != v.end())
        cout << *saveIt++ << " ";
    cout << endl;

}

void bidirectional(){
    cout << "bidirectional iter" << endl;
    vector<int> v(5);
    vector<int>::iterator it = v.begin();
    vector<int>::iterator saveIt = it;
    *it++ = 10;
    *it++ = 11;
    *it++ = 12;
    *it++ = 13;
    *it= 14;
    while (saveIt != v.end())
        cout << *saveIt++ << " ";
    cout << endl;
    do
        cout << *--saveIt << " ";
    while (saveIt != v.begin());
    cout << endl;
}

void randomIter(){
    cout << "random iter" << endl;
    vector<int> v(5);
    vector<int>::iterator it = v.begin();
    *it++ = 10;
    *it++ = 11;
    *it++ = 12;
    *it++ = 13;
    *it= 14;
    for (it = v.begin(); it != v.end(); it++)
        cout << *it << " ";
    cout << endl;
    it = v.begin();
    *(it + 2) = 100;
    for (it = v.begin(); it != v.end(); it++)
        cout << *it << " ";
    cout << endl;
}

void istreamIter(){
    cout << "istream iter" << endl;
    vector<int> v;
    for (int i = 0; i < 5; i++)
        v.push_back(*istream_iterator<int>(std::cin));
    vector<int>::iterator it = v.begin();
    while (it != v.end())
        cout << *it++ << " ";
    cout << endl;
}

void ostreamIter(){
    cout << "ostream iter" << endl;
    vector<int> v;
    for (int i = 0; i < 5; i++)
        v.push_back(*istream_iterator<int>(std::cin));

    ostream_iterator<int> out_it(cout, "\n");
    copy(v.begin(), v.end(), out_it);
}

void reverceIter(){
    cout << "reverce iter" << endl;
    vector<int> v(5);
    vector<int>::iterator it = v.begin();
    *it++ = 10;
    *it++ = 11;
    *it++ = 12;
    *it++ = 13;
    *it= 14;
    for (it = v.begin(); it != v.end(); it++)
        cout << *it << " ";
    cout << endl;
    for (vector<int>::reverse_iterator rit = v.rbegin(); rit != v.rend(); rit++)
        cout << *rit << " ";
    cout << endl;
}

void insertIter(){
    cout << "insert iter" << endl;
    vector<int> v(5);
    back_insert_iterator< vector<int> > it(v);
    *it++ = 10;
    *it++ = 11;
    *it++ = 12;
    *it++ = 13;
    *it= 14;
    vector<int>::iterator iter;
    for (iter= v.begin(); iter != v.end(); iter++)
        cout << *iter << " ";
    cout << endl;

}

int main(){
    inputIterator();
    outIterator();
    forwardIter();
    bidirectional();
    randomIter();
    //istreamIter();
    //ostreamIter();
    reverceIter();
    insertIter();
    return 0;
}
